console.log('Hello World');

				// Loops
//While Loop
// will allow us to repeat an action or an instruction as long as the condition is true.

/*
Syntax:

	while(expression/condition){
		statement
	}
*/ 

// sample
// Create a function called displayMsgToSelf()
	// -display a message to your past self in your console 10 times.
	// Invoke the function 10 times.

// function displayMsgToSelf(){
// 	console.log('dont text her');
// }

// displayMsgToSelf();


			// sample #1 while loop

let count = 10;
while (count !== 0){
	console.log('dont text her')
	count--;
}

/*
1st loop = Count 10
2nd loop = Count 9
3rd loop = Count 10......

9th loop = Count 2
10th loop = Count 1


*/

			// sample #2

// let count1 = 5;

// while (count1 !== 0){
// 	console.log(count1)
// 	count--;
// }


// DO while loop

/*
Syntax:
	do{
		statement
	} while(expression/condition)
*/

			// sample #1

let doWhileCounter = 1

	do{
		console.log(doWhileCounter);
		doWhileCounter++

	} while(doWhileCounter <= 20)


			// sample #2

let number = Number(prompt('give me a numer'));

do {
	console.log('Do while:' + number);
	number +=1;
} while(number < 10)



				// For loop

/*
it consist of three parts:


	Syntax:

		for(initialization; expression/condition; finalExpression)
*/


					// sample #1 FOR loop

for (let count = 0; count <= 20; count++){
	console.log(count);
}



				// sample #2 FOR loop
// accessing array items

let fruits = ['apple', 'durian', 'kiwi', 'pineapple', 'mango', 'orange'];

console.log(fruits[2]);
console.log(fruits.length)
console.log(fruits[fruits.length-1]);

// show all items in array in the console using loop

for(let index = 0; index < fruits.length; index++)
	console.log(fruits[index]);

					// Miniactivity

let country = ['japan', 'russia', 'philippines', 'canada', 'germany']

console.log('my favorite country is: ' + country[2]);
/*console.log(country[country.length-1]);*/

for(let index = 0; index < country.length-1; index++)
	console.log(country[index]);


// for loops accessing elements of a string

			// sample #1 Loops for strings

let myString = 'alex';
// .length it is also a property used in strings

console.log(myString.length); //4


console.log(myString[0]);

// for (let x = 0; x < myString.length; x++){
// 	console.log(myString[x])}

// 		// sample #1 Loops for strings
// // let myName = 'jane';

// // for (let i = 0; 1 < myName.length; i++){
// // 	console.log(myName[i])
// // }




				// Continue and Break statements.
						
						// sample #1
for(let count = 0; count <= 20; count++){
	if(count % 2 === 0){
		continue;
	}
	console.log('continue and break: '  + count);
	if(count > 10){
		break;
	}
}


						// sample #2
let name = 'alexandro';

for (let i = 0; i < name.length; i++){
	console.log(name[i])
	if(name[i].toLowerCase() === 'a'){
		console.log('continue to the new iteration')
		continue;
	}
	
	if (name[i] == 'd'){
		break;
	}
}